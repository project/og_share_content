CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Og share content is a drupal module that gives the user the ability 
to share content between Organic Groups.
Every user in the group can share content with any group he is member of.
"og share content" button will be displayed in the "Node Links" of the node.


REQUIREMENTS
------------

og



INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7 for
    further information.

  * Enable the Og share content module.
  
CONFIGURATION
-------------

* Navigate Configuration » Og share content settings.
* Define og content type - the og group you want to allow share content of him.
* Define which content types in the group the user can share with another group.
* Define whether to limit group parent :
  If the group you selected in "og group content" has og group parent, 
  and you check this field, the user will be able to share content 
  of this group only to groups are in the same parent too. 
  If you don't check this limitatioin the user will be able to share with all
  groups he has permissions to in the site.
 
 
Author
------
Israel Government.
